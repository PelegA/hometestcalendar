<?php
//taken from https://developers.google.com/api-client-library/php/auth/web-app
require __DIR__ . '/vendor/autoload.php';

session_start();

$client = new Google_Client();
$client->setAuthConfigFile('client_secret.json');
$client->setRedirectUri('http://pelegam.myweb.jce.ac.il/calendar/oauth2callback.php');
$client->addScope('https://www.googleapis.com/auth/calendar');

if (! isset($_GET['code'])) { //האם יש אישור של ה-USER לראפליקציה- האם יש קוד מגוגל שהגיע ב-GET
  //כניסה בפעם הראשונה של המשתמש כי לא פתוח session
  $auth_url = $client->createAuthUrl(); //בנייה של URL
  header('Location: ' . filter_var($auth_url, FILTER_SANITIZE_URL)); //הפנייה אל גוגל לאישור 
} else {
  $client->authenticate($_GET['code']); //נותן קוד זמני
  //יצירת קשר ישיר עם ג וגל על מנת לקבל את הקוד הקבוע access_token
  $_SESSION['access_token'] = $client->getAccessToken();//שמירת הקוד הקבוע היא בסשין
  $redirect_uri = 'http://pelegam.myweb.jce.ac.il/calendar';//מפנה לאינדקס
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
}